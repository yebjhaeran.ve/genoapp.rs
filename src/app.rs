use crate::DatasetGenerators;
use std::sync::mpsc;
use std::path::PathBuf;

pub struct GenoApp {
    // Example stuff:
    label: String,
    value: u64,
    picked_path: Option<PathBuf>,
    dataset_running: Option<DatasetGenerators::DatasetAlgorithms>,
    receiver: Option<mpsc::Receiver<Result<(), DatasetGenerators::DatasetErrors>>>,
    errors: Option<DatasetGenerators::DatasetErrors>,
    allert: Option<DatasetGenerators::DatasetAlgorithms>,
}

impl Default for GenoApp {
    fn default() -> Self {
        Self {
            // Example stuff:
            label: "Hello World!".to_owned(),
            value: 300_000_000u64,
            picked_path: None,
            dataset_running: None,
            receiver: None,
            errors: None,
            allert: None,

        }
    }
}

impl GenoApp {
    /// Called once before the first frame.
    pub fn new() -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        Default::default()
    }
}

impl eframe::App for GenoApp {
    /// Called each time the UI needs repainting, which may be many times per second.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        // Put your widgets into a `SidePanel`, `TopBottomPanel`, `CentralPanel`, `Window` or `Area`.
        // For inspiration and more examples, go to https://emilk.github.io/egui
        //
        match self.dataset_running {
            None => (),

            Some(_) => {
                match &self.receiver {
                    None => (),

                    Some(receiver) => {
                        match receiver.recv() {
                            //=|=|=|=|=>
                            Ok(res) => match res {
                                Ok(()) => {
                                    self.dataset_running = None;
                                    self.allert = Some(DatasetGenerators::DatasetAlgorithms::ZeroOneAlgorithm);
                                },
                                Err(e) => {
                                    self.errors = Some(e);
                                }
                            },

                            Err(_) => {
                                self.receiver = None;
                            },
                        } //=|=|=|=|=>
                    }
                }
            }
        }

        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            // The top panel is often a good place for a menu bar:

            egui::menu::bar(ui, |ui| {
                // NOTE: no File->Quit on web pages!
                ui.menu_button("File", |ui| {
                    if ui.button("Quit").clicked() {
                        ctx.send_viewport_cmd(egui::ViewportCommand::Close);
                    }
                });
                ui.add_space(16.0);

                egui::widgets::global_dark_light_mode_buttons(ui);
            });
        }); // ---------------

        egui::CentralPanel::default().show(ctx, |ui| {
            // The central panel the region left after adding TopPanel's and SidePanel's
            ui.heading("Random Dna Generator");

            ui.label("01 format dataset generator");
            ui.horizontal(|ui| {
                ui.label("Set the number of Samples you want: ");
                ui.add(
                    egui::DragValue::new(&mut self.value)
                        .clamp_range(1..=300_000_000)
                        .speed(100000),
                );
            });

            ui.vertical(|ui| {
                ui.horizontal(|ui| {
                    if ui.button("File to save:").clicked() {
                        self.picked_path = rfd::FileDialog::new().save_file();
                    }

                    if let Some(picked_path) = &self.picked_path {
                        ui.horizontal(|ui| {
                            ui.label("Picked file:");
                            ui.monospace(picked_path.as_path().to_str().unwrap());
                        });
                    }
                });
                if let Some(allert) = &self.allert { 
                    ui.vertical(|ui| { 
                        ui.label(allert.to_string());
                    });
                }

                if let Some(picked_path_b) = &self.picked_path {
                    let picked_path = picked_path_b.clone();
                    ui.vertical(|ui| {
                        if ui.button("Generate Dataset").clicked() { //------------>
                            match &self.dataset_running {
                                Some(_dataset_algorithm) => (),

                                None => {
                                    self.receiver = Some(DatasetGenerators::DatasetAlgorithms::binary_dna01_generator_with_threads(
                                        &self.value,
                                        picked_path.to_str().unwrap(),
                                    ));

                                    self.dataset_running = Some(DatasetGenerators::DatasetAlgorithms::ZeroOneAlgorithm);
                                }
                            }
                        } //------------>
                    });
                }
            });
        });
    }
}

fn powered_by_egui_and_eframe(ui: &mut egui::Ui) {
    ui.horizontal(|ui| {
        ui.spacing_mut().item_spacing.x = 0.0;
        ui.label("Powered by ");
        ui.hyperlink_to("egui", "https://github.com/emilk/egui");
        ui.label(" and ");
        ui.hyperlink_to(
            "eframe",
            "https://github.com/emilk/egui/tree/master/crates/eframe",
        );
        ui.label(".");
    });
}
