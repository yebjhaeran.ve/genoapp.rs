use egui::IconData;

use egui::{
    FontFamily::Proportional,
    TextStyle::{self, Body, Button, Heading, Monospace, Name, Small},
};

// When compiling natively:
fn main() -> eframe::Result<()> {
    env_logger::init(); // Log to stderr (if you run with `RUST_LOG=debug`).

    let icon_image = image::open("assets/icon-256.png").unwrap();
    let width = icon_image.width();
    let height = icon_image.height();
    let icon_rgba8 = icon_image.into_rgba8().to_vec();
    let icon_data = IconData {
        rgba: icon_rgba8,
        width,
        height,
    };

    let native_options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default()
            .with_inner_size([400.0, 300.0])
            .with_min_inner_size([300.0, 220.0])
            .with_drag_and_drop(true)
            .with_icon(icon_data),
        ..Default::default()
    };
    eframe::run_native(
        "Genoma And Protein coder and Dataset Generator",
        native_options,
        Box::new(|_| Box::new(ProtonomaDatasetApp::GenoApp::new())),
    )
}
