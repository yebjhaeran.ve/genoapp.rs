#[warn(unused_imports)]
#[warn(dead_code)]
#[warn(non_snake_case)]
use rand::{thread_rng, Rng};
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;
use std::sync::mpsc;
use std::sync::mpsc::Receiver;
use std::thread;
use std::fmt;

#[derive(Debug)]
pub enum DatasetErrors {
    ThreadAllreadyCreated(&'static str),
    FileExists(&'static str),
    FileNotCreated(&'static str),
}

#[derive(Debug)]
pub enum DatasetAlgorithms {
    ZeroOneAlgorithm,
}

impl fmt::Display for DatasetErrors {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { 
        match self {
           DatasetErrors::ThreadAllreadyCreated(string) => write!(f, "{}",string),
           DatasetErrors::FileExists(string) => write!(f, "{}",string),
           DatasetErrors::FileNotCreated(string) => write!(f, "{}",string),
       }
    }
}

impl fmt::Display for DatasetAlgorithms {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { 
        match self {
           DatasetAlgorithms::ZeroOneAlgorithm => write!(f, "{:?}",self),
       }
    }
}

impl DatasetAlgorithms {
    pub fn binary_dna01_generator(
        how_many_letters: &u64,
        path_to_save: &Path,
    ) -> Result<(), DatasetErrors> {
        if let Ok(file) = File::create(path_to_save) {
            let mut file_writter = BufWriter::new(file);
            let mut rng = thread_rng();
            let iterations = how_many_letters / 4;

            for _ in 1..=iterations {
                let P1 = (rng.gen_range(0..=3) as u8) << 6u8;
                let P2 = (rng.gen_range(0..=3) as u8) << 4u8;
                let P3 = (rng.gen_range(0..=3) as u8) << 2u8;
                let P4 = rng.gen_range(0..=3) as u8;

                let Line = P1 | P2 | P3 | P4;
                file_writter.write(&[Line]).unwrap();
            }
            file_writter.flush().unwrap();
            return Ok(());
        }
        return Err(DatasetErrors::FileNotCreated("Path Does not Exist"));
    }

    pub fn binary_dna01_generator_with_threads(
        how_many_letters: &u64,
        path_to_save: &str,
    ) -> Receiver<Result<(), DatasetErrors>> {
        let (tx, rx) = mpsc::channel::<Result<(), DatasetErrors>>();
        let how_many_letters_clone = *how_many_letters;
        let path_to_save_clone = path_to_save.clone().to_owned();

        thread::spawn(move || {
            let result = Self::binary_dna01_generator(
                &how_many_letters_clone,
                &Path::new(&path_to_save_clone),
            );
            tx.send(result).unwrap();
        });

        return rx;
    }
}
