# eframe template

[![dependency status](https://deps.rs/repo/github/emilk/eframe_template/status.svg)](https://deps.rs/repo/github/emilk/eframe_template)
[![Build Status](https://github.com/emilk/eframe_template/workflows/CI/badge.svg)](https://github.com/emilk/eframe_template/actions?workflow=CI)

This is a app designed to create random dna and translate dna sequences into proteins all in binary form build with rust and parallelization in mind

## Getting started

Download rust and run `cargo rr` for running it with the `--release` flag.

### TO-DO'S
- [] complete the parallel backend for running the algorithms
- [] add the translation algorithm 
- [] visual design 
- [] add cuda support











































